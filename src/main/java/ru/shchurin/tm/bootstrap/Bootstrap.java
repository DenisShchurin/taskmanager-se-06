package ru.shchurin.tm.bootstrap;

import ru.shchurin.tm.command.*;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.repository.ProjectRepository;
import ru.shchurin.tm.repository.TaskRepository;
import ru.shchurin.tm.repository.UserRepository;
import ru.shchurin.tm.service.*;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.*;

public class Bootstrap {
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final UserRepository userRepository = new UserRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final TaskService taskService = new TaskService(taskRepository);
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final UserService userService = new UserService(userRepository);
    private final Map<String, AbstractCommand> commands = new HashMap<>();

    private User currentUser;

    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.getCommand();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private void init() throws CommandCorruptException, ConsoleLoginException, ConsoleHashPasswordException, AlreadyExistsException {
        registry(new HelpCommand());
        registry(new ExitCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectUpdateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskUpdateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveCommand());
        registry(new TasksOfProjectCommand());
        registry(new UserAuthorizationCommand());
        registry(new UserEditProfileCommand());
        registry(new UserEndSessionCommand());
        registry(new UserRegistrationCommand());
        registry(new UserShowProfileCommand());
        registry(new UserUpdatePasswordCommand());
        registry(new UserListCommand());
        userService.persist(new User("Admin", "e3afed0047b08059d0fada10f400c1e5" , Role.ROLE_ADMIN));
        userService.persist(new User("User", "8f9bfe9d1345237cb3b2b205864da075", Role.ROLE_USER));
    }

    public void start() throws Exception {
        try {
            init();
        } catch (CommandCorruptException e) {
            System.out.println(e.getMessage());
            return;
        }

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = ConsoleUtil.getStringFromConsole();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        if(!abstractCommand.isSafe() && currentUser == null)
            throw new UserNotAuthorized();
        if(!abstractCommand.isSafe() && currentUser != null && !abstractCommand.getRoles().contains(currentUser.getRole()))
            throw new UserHasNotAccess();
        abstractCommand.execute();
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public UserService getUserService() {
        return userService;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
