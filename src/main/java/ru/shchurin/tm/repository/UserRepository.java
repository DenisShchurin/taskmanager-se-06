package ru.shchurin.tm.repository;

import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AlreadyExistsException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UserRepository {
    private Map<String, User> users = new HashMap<>();

    public Collection<User> findAll() {
        return users.values();
    }

    public User findOne(String id) {
        return users.get(id);
    }

    public void persist(User user) throws AlreadyExistsException {
        if (users.containsKey(user.getId())) {
            throw new AlreadyExistsException();
        }
        users.put(user.getId(), user);
    }

    public void merge(User user) {
        users.put(user.getId(), user);
    }

    public void remove(String id) {
        users.remove(id);
    }

    public void removeAll() {
        users.clear();
    }

    public void removeByLogin(String login) {
        for (Iterator<Map.Entry<String, User>> it = users.entrySet().iterator(); it.hasNext();) {
            Map.Entry<String, User> e = it.next();
            if (login.equals(e.getValue().getLogin())) {
                it.remove();
            }
        }
    }

    public boolean updatePassword(String login, String hashPassword, String newHashPassword) {
        for (User user : users.values()) {
            if (user.getLogin().equals(login) && user.getHashPassword().equals(hashPassword)) {
                user.setHashPassword(newHashPassword);
                return true;
            }
        }
        return false;
    }
}
