package ru.shchurin.tm.repository;

import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.exception.*;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> tasks = new HashMap<>();

    public List<Task> findAll(String userId) {
        List<Task> userTasks = new ArrayList<>();
        for (Task task : tasks.values()) {
            if (userId.equals(task.getUserId()))
                userTasks.add(task);
        }
        return userTasks;
    }

    public Task findOne(String userId, String id) {
        Task task = tasks.get(id);
        if (task.getUserId().equals(userId)) {
            return task;
        } else {
            return null;
        }
    }

    public void persist(Task task) throws AlreadyExistsException {
        if (tasks.containsKey(task.getId()))
            throw new AlreadyExistsException();
        tasks.put(task.getId(), task);
    }

    public void merge(Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(String userId, String id) {
        Task task = tasks.get(id);
        if (task.getUserId().equals(userId))
            tasks.remove(id);
    }

    public void removeAll(String userId) {
        for (Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> e = it.next();
            if (userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }

    public void removeByName(String userId, String name) {
        for (Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> e = it.next();
            if (name.equals(e.getValue().getName()) && userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }
}
