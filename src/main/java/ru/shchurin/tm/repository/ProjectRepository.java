package ru.shchurin.tm.repository;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.exception.*;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projects = new HashMap<>();

    public List<Project> findAll(String userId) {
        List<Project> userProjects = new ArrayList<>();
        for (Project project : projects.values()) {
            if (userId.equals(project.getUserId()))
                userProjects.add(project);
        }
        return userProjects;
    }

    public Project findOne(String userId, String id) {
        Project project = projects.get(id);
        if (project.getUserId().equals(userId)) {
            return project;
        } else {
            return null;
        }
    }

    public void persist(Project project) throws AlreadyExistsException {
        if (projects.containsKey(project.getId()))
            throw new AlreadyExistsException();
        projects.put(project.getId(), project);
    }

    public void merge(Project project) {
        projects.put(project.getId(), project);
    }

    public void remove(String userId, String id) {
        Project project = projects.get(id);
        if (project.getUserId().equals(userId))
            projects.remove(id);
    }

    public void removeAll(String userId) {
        for (Iterator<Map.Entry<String, Project>> it = projects.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Project> e = it.next();
            if (userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }

    public void removeByName(String userId, String name) {
        for (Iterator<Map.Entry<String, Project>> it = projects.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Project> e = it.next();
            if (name.equals(e.getValue().getName()) && userId.equals(e.getValue().getUserId()))
                it.remove();
        }
    }
}
