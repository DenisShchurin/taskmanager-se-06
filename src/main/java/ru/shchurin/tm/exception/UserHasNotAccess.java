package ru.shchurin.tm.exception;

public class UserHasNotAccess extends Exception {
    public UserHasNotAccess() {
        super("You have not access");
    }
}
