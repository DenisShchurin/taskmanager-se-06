package ru.shchurin.tm.exception;

public class AlreadyExistsException extends Exception{
    public AlreadyExistsException() {
        super("THIS TASK ALREADY EXISTS");
    }
}
