package ru.shchurin.tm.entity;

import java.util.UUID;

public class User {
    private String id = UUID.randomUUID().toString();
    private String login;
    private String hashPassword;
    private Role role;

    public User(String login, String hashPassword) {
        this.login = login;
        this.hashPassword = hashPassword;
        this.role = Role.ROLE_USER;
    }

    public User(String login, String hashPassword, Role role) {
        this.login = login;
        this.hashPassword = hashPassword;
        this.role = role;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setHashPassword(String hashPassword) {
        this.hashPassword = hashPassword;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", hashPassword='" + hashPassword + '\'' +
                ", role=" + role +
                '}';
    }
}
