package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.repository.UserRepository;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    public User findOne(String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        return userRepository.findOne(id);
    }

    public void persist(User user) throws AlreadyExistsException, ConsoleLoginException, ConsoleHashPasswordException {
        if (user == null) {
            return;
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new ConsoleLoginException();
        }
        if (user.getHashPassword() == null || user.getHashPassword().isEmpty()) {
            throw new ConsoleHashPasswordException();
        }
        userRepository.persist(user);
    }

    public void merge(User user) throws ConsoleLoginException {
        if (user == null) {
            return;
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new ConsoleLoginException();
        }
        userRepository.merge(user);
    }

    public void remove(String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        userRepository.remove(id);
    }

    public void removeAll() {
        userRepository.removeAll();
    }

    public void removeByLogin(String login) throws ConsoleLoginException {
        if (login == null || login.isEmpty()) {
            throw new ConsoleLoginException();
        }
        userRepository.removeByLogin(login);
    }

    public String getHashOfPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }

    public User authoriseUser(String login, String hashOfPassword) throws ConsoleLoginException, ConsoleHashPasswordException {
        if (login == null || login.isEmpty()) {
            throw new ConsoleLoginException();
        }
        if (hashOfPassword == null || hashOfPassword.isEmpty()) {
            throw new ConsoleHashPasswordException();
        }
        Collection<User> users = userRepository.findAll();
        for (User user : users) {
            if (user.getLogin().equals(login) && user.getHashPassword().equals(hashOfPassword)) {
                return user;
            }
        }
        return null;
    }

    public boolean updatePassword(String login, String hashPassword, String newHashPassword) {
        return userRepository.updatePassword(login, hashPassword, newHashPassword);
    }
}
