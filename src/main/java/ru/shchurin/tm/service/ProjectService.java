package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.repository.ProjectRepository;
import ru.shchurin.tm.exception.*;
import java.util.List;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> findAll(String userId) {
        return projectRepository.findAll(userId);
    }

    public Project findOne(String userId, String id) throws ConsoleIdException, ProjectNotFoundException {
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        Project project = projectRepository.findOne(userId, id);
        if (project == null)
            throw  new ProjectNotFoundException();
        return project;
    }

    public void persist(Project project) throws AlreadyExistsException, ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (project == null)
            return;
        if (project.getName() == null || project.getName().isEmpty())
            throw new ConsoleNameException();
        if (project.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (project.getEndDate() == null)
            throw new ConsoleEndDateException();
        projectRepository.persist(project);
    }

    public void merge(Project project) throws ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (project == null)
            return;
        if (project.getName() == null || project.getName().isEmpty())
            throw new ConsoleNameException();
        if (project.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (project.getEndDate() == null)
            throw new ConsoleEndDateException();
        projectRepository.merge(project);
    }

    public void remove(String userId, String id) throws ConsoleIdException {
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        projectRepository.remove(userId, id);
    }

    public void removeAll(String userId) {
        projectRepository.removeAll(userId);
    }

    public void removeByName(String userId, String name) throws ConsoleNameException {
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        projectRepository.removeByName(userId, name);
    }
}
