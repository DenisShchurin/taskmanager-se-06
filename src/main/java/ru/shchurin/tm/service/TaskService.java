package ru.shchurin.tm.service;

import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.repository.TaskRepository;
import ru.shchurin.tm.exception.*;
import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll(String userId) {
        return taskRepository.findAll(userId);
    }

    public Task findOne(String userId, String id) throws ConsoleIdException, TaskNotFoundException {
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        Task task = taskRepository.findOne(userId, id);
        if (task == null)
            throw new TaskNotFoundException();
        return task;
    }

    public void persist(Task task) throws AlreadyExistsException, ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (task == null)
            return;
        if (task.getName() == null || task.getName().isEmpty())
            throw new ConsoleNameException();
        if (task.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (task.getEndDate() == null)
            throw new ConsoleEndDateException();
        taskRepository.persist(task);
    }

    public void merge(Task task) throws ConsoleNameException, ConsoleStartDateException, ConsoleEndDateException {
        if (task == null)
            return;
        if (task.getName() == null || task.getName().isEmpty())
            throw new ConsoleNameException();
        if (task.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (task.getEndDate() == null)
            throw new ConsoleEndDateException();
        taskRepository.merge(task);
    }

    public void remove(String userId, String id) throws ConsoleIdException {
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        taskRepository.remove(userId, id);
    }

    public void removeAll(String userId) {
        taskRepository.removeAll(userId);
    }

    public void removeByName(String userId, String name) throws ConsoleNameException {
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        taskRepository.removeByName(userId, name);
    }
}
