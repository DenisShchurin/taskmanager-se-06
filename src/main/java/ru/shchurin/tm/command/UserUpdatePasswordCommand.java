package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public class UserUpdatePasswordCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "user-update-password";
    }

    @Override
    public String getDescription() {
        return "Update user password";
    }

    @Override
    public void execute() throws Exception {
        User currentUser = bootstrap.getCurrentUser();
        System.out.println("[USER PASSWORD UPDATE]");
        System.out.println("ENTER PASSWORD:");
        String newPassword = ConsoleUtil.getStringFromConsole();
        String newHashPassword = bootstrap.getUserService().getHashOfPassword(newPassword);
        boolean update = bootstrap.getUserService().updatePassword(currentUser.getLogin(), currentUser.getHashPassword(), newHashPassword);
        if (update) {
            System.out.println("USER PASSWORD UPDATED");
        } else {
            System.out.println("USER PASSWORD IS NOT UPDATED");
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
