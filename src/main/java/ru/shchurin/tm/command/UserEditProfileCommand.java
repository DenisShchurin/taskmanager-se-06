package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AccessDeniedException;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public class UserEditProfileCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "user-edit-profile";
    }

    @Override
    public String getDescription() {
        return "Edit user profile";
    }

    @Override
    public void execute() throws Exception {
        User currentUser = bootstrap.getCurrentUser();
        System.out.println("[EDIT USER PROFILE]");
        System.out.println("ENTER LOGIN:");
        String login = ConsoleUtil.getStringFromConsole();
        currentUser.setLogin(login);
        bootstrap.getUserService().merge(currentUser);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
