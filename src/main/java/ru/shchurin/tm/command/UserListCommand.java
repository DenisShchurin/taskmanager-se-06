package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AccessDeniedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UserListCommand extends AbstractCommand {
    private final boolean safe = false;
    private final ArrayList<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_ADMIN));

    @Override
    public String getCommand() {
        return "user-list";
    }

    @Override
    public String getDescription() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        User currentUser = bootstrap.getCurrentUser();
        System.out.println("[USER LIST]");
        int index = 1;
        for (User user: bootstrap.getUserService().findAll()) {
            System.out.println(index++ + ". " + user);
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
