package ru.shchurin.tm.command;

import ru.shchurin.tm.entity.Role;
import java.util.ArrayList;
import java.util.List;

public class ExitCommand extends AbstractCommand {
    private final boolean safe = true;
    private final ArrayList<Role> roles = new ArrayList<>();

    @Override
    public String getCommand() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "exit the application";
    }

    @Override
    public void execute() {
        System.out.println("GOOD BYE");
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
